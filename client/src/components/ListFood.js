import styled from "styled-components"
import Food from './Food'


const ListFood = (props) => {
  
  var data = props.data;

  return (
    <Container>
      <Row>
        <Title>{(props.title) === '' ? "Type" : props.title}</Title>
        <Hr/>
      </Row>
        <FoodContainer>
          {(data.length > 0) && data.map((food) => <Food key={food.id} food={food} onDisplay={() => props.onDisplay(food)} onAdd={() => props.onAddOneItem(food)}/>)}
        </FoodContainer>
    </Container>
  )
}

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
`;
const Hr = styled.div`
  width: 100%;
  height: 1px;
  border-radius: 100px;
  background-color: #d9d9d9;
`;
const Container = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0px 10px;
  overflow: auto;
`;
const Title = styled.div`
  display: flex;
  width: 20%;
  flex-direction: row;
  align-items: center;
  text-align: center;
  margin: 0px 40px 0px 20px;
  font-weight: bold;
`;

const FoodContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  height: 100%;
  align-items: left;
  overflow-y: scroll;
  &::-webkit-scrollbar {
    width: 5px;
  }
  /* Track */
  &::-webkit-scrollbar-track {
    background: transparent;
  }
  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: #80808087;
    border-radius: 3px;
  }
  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #757575;
  }
}
`;
export default ListFood;
