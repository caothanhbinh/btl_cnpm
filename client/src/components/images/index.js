import noodle from './noodle.png';
import burger from './burger.png';
import sandwich from './sandwich.png';
import seafood from './seafood.png';
import blue_cake from './blue_cake.png'
import choco_cake from './choco_cake.png'
import love_cake from './love_cake.png'
import juice from './juice.png'
import orange from './orange.png'
import pink_cake from './pink_cake.png'
import scream_cake from './scream_cake.png'
import watermelon from './watermelon.png'
import coca from './coca.png'
import pepsi from './pepsi.png'
import redbull from './redbull.png'
import cupcake from './cupcake.png'
import shrimp from './shrimp.png'
import clam from './clam.png'
import lobster from './Lobster.png'
import pasta from './pasta.png'
import spaghetti from './spaghetti.png'
import fastfood from './fastfood.png'
import chicken from './chicken.png'
import chip from './chip.png'
import water from './water.png'
import pizza from './pizza.png'
import hotdog from './hotdog.png'

const images = {
  noodle, burger, sandwich, seafood, blue_cake, choco_cake, love_cake, juice, orange, pink_cake, scream_cake, watermelon , coca, pepsi, redbull,cupcake,shrimp,clam,lobster,pasta,spaghetti,fastfood,chicken,chip,water,pizza,hotdog
}

export default images;