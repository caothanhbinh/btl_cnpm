import React from 'react';
import Button from "./Button";
import styled from 'styled-components';

const Cart = (props) => {
  const { countCartItems, cartItems, onAdd, onRemove } = props;
  const itemsPrice = cartItems.reduce((a, c) => a + (c.price + parseInt(c.sideDishsChecked.reduce((x, y) => x + y.price, 0))) * c.quantity, 0);
  const taxPrice = Math.round(itemsPrice) / 10
  const totalPrice = itemsPrice + taxPrice;

  return (
    <Container show={props.show}>
      <Title>
        <Row>
          <div>
            <i className="fa fa-shopping-cart" aria-hidden="true"/>
            <span>Cart Items ({countCartItems})</span>
          </div>
          <i className="fa fa-times" aria-hidden="true" onClick={props.onDisplay}/>
        </Row>
      </Title>
        {cartItems.length === 0 && <div className="is-empty">Cart Is Empty</div>}
      <CartContainer>
        {cartItems.map((item) => (
          <Item key={item.cartId}>
            <img src={item.url} alt="avt"/>
            <Content>
              <Col>
                <div className="name">{item.name}</div>
                {item.sideDishsChecked.map((sideDish) => 
                  <Row key={sideDish.name}>
                    <span>{sideDish.name}</span>
                    <span className='price-mini'>{"$" + sideDish.price*item.quantity}</span>
                  </Row>
                )}
                <Row>
                  <Quantity>
                    <Button onClick={() => onRemove(item)} text="-" buttonStyle="btn--decrease" buttonSize="btn--small"/>
                    <ItemQuantity> {item.quantity} </ItemQuantity>
                    <Button onClick={() => onAdd(item)} text="+" buttonStyle="btn--increase" buttonSize="btn--small"/>
                  </Quantity>
                  <div className='price'>
                    {"$"+item.price*item.quantity}
                  </div>
                </Row>
              </Col>
            </Content>
          </Item>
        ))}
      </CartContainer>
      <Payment>
        <Row>
          <span className="title">Total:</span>
          <span className="price-large">{"$"+totalPrice}</span>
        </Row>
        <Row marginBottom="10px">
          <span className="tax">{"(Incl.tax 10% = $"+taxPrice+")"}</span>
        </Row>
        <Button buttonSize="btn--large" text="Payment" onClick={props.payment}/>
      </Payment>
    </Container>
  )

}
const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 30%;
  border-radius: 5px;
  box-shadow: -5px 0 10px -5px #d9d9d9;
  padding: 10px;
  & .is-empty {
    margin: auto;
    text-align: center;
    font-size: 20px;
    color: #00000070;
  }
  @media (max-width: 768px) {
    left: ${props => props.show ? '0' : '-100%'};
    background: white;
    width: 100%;
    height: 99%;
    position: absolute;
    transition: all 0.5s ease;
  }
`;

const Title = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-bottom: 20px;
  justify-content: space-between;
  width: 100%;
  & i {
    color: #ed0000;
    font-size: 28px;
    font-weight: bold;
  }
  & .fa.fa-times {
    color: black;
    display: none;
    :hover {
      cursor: pointer;
    }
  }
  & div {
    color: red;
    font-size: 20px;
    font-weight: bold;
  }
  & .fa.fa-shopping-cart {
    margin-right: 15px;
  }
  @media (max-width: 768px) {
    & .fa.fa-times {
      color: black;
      display: inline;
      :hover {
        cursor: pointer;
      }
    }
  }
`;
const CartContainer = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 100%;
  padding-right:3px;
  box-sizing: border-box;
  overflow-y: scroll;
  overflow-x: hidden;
`;
const Content = styled.div`
  & .name {
    font-weight: bold;
  }
  width: 100%;
`;

const Col = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  margin-bottom: ${props => props.marginBottom ? props.marginBottom : ""};
  & .price {
    color: red;
    font-weight: bold;
  }
  & .price-large{
    color: red;
    font-weight: bold;
    font-size: 20px;
  }
  & .price-mini {
    color: red;
  }
  & .title {
    color: #333333;
    font-weight: bold;
    font-size: 20px;
  }
  & .tax {
    width: 100%;
    text-align: right;
  }
`;

const Item = styled.div`
    display: flex;
    flex-direction: row;
    padding: 5px;
    border-radius: 5px;
    border: 1px solid #cccccc;
    margin-bottom: 10px;
    & img {
      height: 50px;
      width: 50px;
      margin: auto 10px;
    }
`;
const Quantity = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

const Payment = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

const ItemQuantity = styled.span`
  margin: 10px;
`

export default Cart;
