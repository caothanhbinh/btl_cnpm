import styled from "styled-components";

const TopBar = (props) => {
    return (
        <Container>
          <div>  
            <Icon className={`fa ${props.icon}`} aria-hidden="true"/>
            <Title>{props.title}</Title>
          </div>
          <CartButton className="fa fa-shopping-cart" aria-hidden="true" onClick={props.onDisplay}><span className='countCart'>({props.numItem})</span></CartButton>
        </Container>
    )
}

const Container = styled.div` 
    background-color: #FFFFFF;
    border-radius: 3px;
    height: 45px;
    display: flex;
    justify-content: space-between;
    align-items: center;
    & .fa.fa-shopping-cart {
      display: none;
      :hover {
        cursor: pointer;
      }
    }
    @media (max-width: 768px) {
      & .fa.fa-shopping-cart {
        display: inline;
        :hover {
          cursor: pointer;
        }
      }
    }
`;

const Icon = styled.i`
  padding: 5px;
  margin-left: 10px;
  background-color: #2C3A57;
  border-radius: 7px;
  cursor: pointer;
  color: white;
  text-align: center;
  font-size: 20px;
  width: 30px;
  height: 30px;
`;
const Title = styled.span`
  padding: 5px;
  margin-left: 5px;
  border-radius: 7px;
  color: #2C3A57;
  text-align: center;
  font-size: 17px;
  font-weight: bold;
`;

const CartButton = styled.i`
  padding: 5px;
  margin-right: 10px;
  border-radius: 7px;
  cursor: pointer;
  color: red;
  text-align: center;
  font-size: 25px;
  & .countCart{
    font-size: 18px;
  }
`;
export default TopBar;