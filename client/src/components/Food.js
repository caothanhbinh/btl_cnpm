import React from 'react'
import styled from 'styled-components';

const Food = (props) => {
  return (
    <InforFood>
        <img src={props.food.url} width="100px" alt="food" onClick={props.onDisplay}/>
      <div className="content"> 
        <div className="food-detail">
          <div className="food-name">{props.food.name}</div>
          <div className="food-price">{"$"+props.food.price}</div>
        </div>
        <i className="fa fa-shopping-cart add-button" onClick={props.onAdd} aria-hidden="true"></i>
      </div>
    </InforFood>
  )
}
const InforFood = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  width: 28%;
  margin-top: 15px;
  margin-left: 4%;
  align-self: center;
  background-color: #F8F8F8;
  border-radius: 10px;
  box-shadow: 2px -2px 10px #d9d9d9;
  @media (max-width: 768px) {
    width: 44%;
  }
  & img {
    width: 100px;
    height: 100px;
    margin: 10px auto;
    cursor: pointer;
  }
  & .content {
    background-color: white;
    border-bottom-left-radius: 10px;
    border-bottom-right-radius: 10px;
    padding: 10px;
    & .food-detail {
      & .food-name {
        font-weight: bold;
        border-bottom: solid 1px #cccccc;
        margin-bottom: 10px;
      }
      & .food-price {
        font-weight: bold;
        color: red;
        margin: 5px 0px;
      }
    }
    & .add-button {
      position: absolute;
      right: 10px;
      bottom: 10px;
      color: white;
      line-height: 25px;
      text-align: center;
      width: 25px;
      height: 25px;
      background-color: red;
      border-radius: 3px;
      cursor: pointer;

      &:hover {
        color: red;
        background-color: white;
        transition: all 0.5s;
      }
    }
  }
`;



export default Food;
