const STYLE = ['btn--add', 'btn--increase', 'btn--decrease'];
const SIZE = ['btn--square', 'btn--large','btn--small', 'btn--medium'];

const Button = (props) => {
  const checkButtonStyle = STYLE.includes(props.buttonStyle) ? props.buttonStyle : STYLE[0]
  const checkButtonSize = SIZE.includes(props.buttonSize) ? props.buttonSize : SIZE[0]
  return (
    <button
      className={`Btn ${checkButtonStyle} ${checkButtonSize}`}
      onClick={props.onClick}>
      {props.text}
    </button>
  )
}

export default Button