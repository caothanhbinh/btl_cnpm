import React, { useRef, useState } from 'react';
import styled from 'styled-components';

const ListType = (props) => {  
  var dataType = props.data;
  let scrl = useRef(null);
  const [scrollX, setscrollX] = useState(0);
  const [scrolEnd, setscrolEnd] = useState(false);
  const [targetId, setTargetId] = useState(-1);
  const slide = (shift) => {
    scrl.current.scrollLeft += shift;
    setscrollX(scrollX + shift);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  const scrollCheck = () => {
    setscrollX(scrl.current.scrollLeft);
    if (
      Math.floor(scrl.current.scrollWidth - scrl.current.scrollLeft) <=
      scrl.current.offsetWidth
    ) {
      setscrolEnd(true);
    } else {
      setscrolEnd(false);
    }
  };
  const diplayFoods = (type) => {
    props.onDisplay(type);
    setTargetId(type.id);
  }
  return (
    <Row>
      {scrollX !== 0 && (
      <ButtonScroll onClick={() => slide(-120)}><i className="fa fa-caret-left" aria-hidden="true"></i></ButtonScroll>
      )}
        <Container ref={scrl} onScroll={scrollCheck}>
          {(dataType.length > 0) && 
            dataType.map((type) => 
              <FoodType key={type.id} 
              style={targetId === type.id ? {
                backgroundColor: "#222538",
                color: 'white'
              } : {}} 
              onClick={() => diplayFoods(type)}>
                <div className='image'>
                  <img src={type.url} alt="type food"/>
                </div>
                <div className="typeName">{type.name}</div>
              </FoodType>
          )}
        </Container>
      {!scrolEnd && (
      <ButtonScroll onClick={() => slide(120)}><i className="fa fa-caret-right" aria-hidden="true"></i></ButtonScroll>
      )}
    </Row>
  )
}

const FoodType = styled.div`
    display: flex;
    flex-direction: column;
    margin: 0px 10px;
    border-radius: 10px;
    border-color: #e1e1e1;
    border-style: solid;
    border-width: 1px;
    background-color: white;
    padding: 10px;
    &:hover {
        background-color: #222538;
        & .typeName {
            color: white;
        }
        cursor: pointer;
    }
    & img {
      max-width: 100%;
    }
    & .image {
      padding: 0px 10px;
      width: 70px;
      height: 50px;
    }
    & .typeName {
        margin-top: 5px;
        text-align: center;
        font-weight: bold;
        font-size: 14px;
    }
`;
const Row = styled.div`
  display: flex;
  flex-direction: row;
`;
const Container = styled.div`
  display: flex;
  flex-direction: row;
  padding: 10px;
  width: 100%;
  overflow-y: hidden;
  overflow-x: scroll;
  scroll-behavior: smooth;
  &::-webkit-scrollbar {
    height: 5px;
    display: none;
  }
  /* Track */
  &::-webkit-scrollbar-track {
    background: transparent;
  }
  /* Handle */
  &::-webkit-scrollbar-thumb {
    background: gray;
    border-radius: 3px;
  }
  /* Handle on hover */
  &::-webkit-scrollbar-thumb:hover {
    background: #757575;
  }
`;
const ButtonScroll = styled.button`
  border: none;
  background-color: transparent;
  :hover {
    cursor: pointer;
    color: #00001a;
  }
  color: gray;
  & .fa-caret-left {
    font-size: 50px;
  }
  & .fa-caret-right {
    font-size: 50px;
  }
`;
export default ListType;
