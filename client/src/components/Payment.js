import React, { useEffect, useState } from 'react'
import { Form, Modal, ButtonGroup, Button, OverlayTrigger, Popover } from 'react-bootstrap';
import styled from 'styled-components';
import PropTypes from "prop-types";

const Payment = (props) => {
  const [show, setShow] = useState(false);
  const [method, setMethod] = useState(1);
  const itemsPrice = props.cartItems.reduce((a, c) => a + (c.price + parseInt(c.sideDishsChecked.reduce((x, y) => x + y.price, 0))) * c.quantity, 0);
  const taxPrice = Math.round(itemsPrice) / 10
  const totalPrice = itemsPrice + taxPrice;
  useEffect(() => {
    setShow(props.show);
    var x = document.getElementsByClassName("checkbox");
    for(var i=0; i< x.length; i++) {
      x[i].checked = false;
    }
  }, [props.show]);

  const handleClose = () => {
    setShow(false);
    props.onClose(false);
  }

  const changeMethod = (method) => {
    setMethod(method)
  }

  return (
    <PopupItem show={show} onHide={handleClose}
      dialogClassName="my-modal"
      aria-labelledby="contained-modal-title-vcenter"
      centered>
      <Modal.Header closeButton>
        <Modal.Title>Payment</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Infor>
          <Total mobile={1}>
              <Row>
                <span className="title">({props.numItem}) Item(s)
                  <OverlayTrigger
                    trigger="click"
                    placement='bottom'
                    overlay={
                      <Popover>
                        <Popover.Header as="h3">Cart</Popover.Header>
                        <Popover.Body>
                          <CartItems mobile={1}>
                            {props.cartItems.map((item) => (
                              <Item expand={1} key={item.cartId}>
                                <img src={item.url} alt="avt"/>
                                <Content>
                                  <Col>
                                    <div className="name">{item.name}</div>
                                    {item.sideDishsChecked.map((sideDish) => 
                                      <Row key={sideDish.name}>
                                        <span>{sideDish.name}</span>
                                        <span className='price-mini'>{"$" + sideDish.price*item.quantity}</span>
                                      </Row>
                                    )}
                                    <Row>
                                      <Quantity>
                                        Quantity: {item.quantity}
                                      </Quantity>
                                      <div className='price'>
                                        {"$"+item.price*item.quantity}
                                      </div>
                                    </Row>
                                  </Col>
                                </Content>
                              </Item>
                            ))}
                          </CartItems>
                        </Popover.Body>
                      </Popover>
                    }
                  >
                    <ExpandBtn> (Expand)</ExpandBtn>
                  </OverlayTrigger>
                </span>
              </Row>
              <Row>
                <span className="title">Total:</span>
                <span className="price-large">{"$"+totalPrice}</span>
              </Row>
              <Row>
                <span className="tax">{"(Incl.tax 10% = $"+taxPrice+")"}</span>
              </Row>
              <hr/>
            </Total>
            <FormLabel>Payment Method</FormLabel>
            <ButtonGroup aria-label="Basic example" style={{width: '100%'}}>
              <MethodItem active={method === 1} onClick={() => changeMethod(1)}>
                <i className="fa fa-credit-card" aria-hidden="true"/>
                <Title>Credit Card</Title>
              </MethodItem>
              <MethodItem active={method === 2} onClick={() => changeMethod(2)}>
                <i className="fa fa-paypal" aria-hidden="true"/>
                <Title>Paypal</Title>
              </MethodItem>
              <MethodItem active={method === 3} onClick={() => changeMethod(3)}>
                <i className="fa fa-university" aria-hidden="true"/>
                <Title>Net Banking</Title>
              </MethodItem>
            </ButtonGroup>
            <hr/>
            {method === 1 ? (
            <Form>
              <Form.Group className="mb-3" controlId="formBasicCardNumber">
                <FormLabel>Card Number</FormLabel>
                <Form.Control type="text" placeholder="Your Card Number" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="formBasicCardName">
                <FormLabel>Card Name</FormLabel>
                <Form.Control type="text" placeholder="Your Card Name" />
              </Form.Group>
              <Row>
                <FormGroup className="expDate" controlId="formBasicDate">
                  <FormLabel>Expiration Date</FormLabel>
                  <Form.Control type="month" placeholder="Expiration Date" />
                </FormGroup>
                <Form.Group className="cvv" controlId="formBasicCVV">
                  <FormLabel>CVV</FormLabel>
                  <Form.Control type="text"/>
                </Form.Group>
              </Row>
              <Btn className="d-grid gap-2"><Button>Confirm Payment</Button></Btn>
            </Form>) : <></>}
            {method === 2 ? (
            <Form>
              <FormLabel>Select your paypal account type</FormLabel>
              <Form.Group className="mb-3" controlId="formBasicCardNumber">
                <Form.Check
                  inline
                  label="Domestic"
                  name="group1"
                  type='radio'
                />
                <Form.Check
                  inline
                  label="International"
                  name="group1"
                  type='radio'
                />
              </Form.Group>
              <Form.Text className="text-muted">
                Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order.
              </Form.Text>
              <Btn className="d-grid gap-2"><Button><i className="fa fa-paypal" aria-hidden="true"/>{' Login to my Paypal'}</Button></Btn>
            </Form>) : <></>}
            {method === 3 ? (
            <Form>
              <Form.Group className="mb-3" controlId="formBasicCardNumber">
                <FormLabel>Select Your Bank</FormLabel>
                <Form.Select aria-label="Default select example">
                  <option>-- Please select your bank --</option>
                  <option value="1">Ngân hàng Phương Đông (OCB)</option>
                  <option value="2">Ngân hàng NN&PT Nông thôn Việt Nam (Agribank)</option>
                  <option value="3">Ngân hàng Quân Đội (MB)</option>
                </Form.Select>
              </Form.Group>
              <Form.Text className="text-muted">
                Note: After clicking on the button, you will be directed to a secure gateway for payment. After completing the payment process, you will be redirected back to the website to view details of your order.
              </Form.Text>
              <Btn className="d-grid gap-2"><Button>Proceed Payment</Button></Btn>
            </Form>) : <></>}
            
          </Infor>
          <Cart>
            <CartItems>
              {props.cartItems.map((item) => (
                <Item key={item.cartId}>
                  <img src={item.url} alt="avt"/>
                  <Content>
                    <Col>
                      <div className="name">{item.name}</div>
                      {item.sideDishsChecked.map((sideDish) => 
                        <Row key={sideDish.name}>
                          <span>{sideDish.name}</span>
                          <span className='price-mini'>{"$" + sideDish.price*item.quantity}</span>
                        </Row>
                      )}
                      <Row>
                        <Quantity>
                          Quantity: {item.quantity}
                        </Quantity>
                        <div className='price'>
                          {"$"+item.price*item.quantity}
                        </div>
                      </Row>
                    </Col>
                  </Content>
                </Item>
              ))}
            </CartItems>
            <Total>
              <Row>
                <span className="title">Total:</span>
                <span className="price-large">{"$"+totalPrice}</span>
              </Row>
              <Row marginBottom="10px">
                <span className="tax">{"(Incl.tax 10% = $"+taxPrice+")"}</span>
              </Row>
            </Total>
          </Cart>
        </Container>
      </Modal.Body>
    </PopupItem>
  );
}

Payment.propTypes = {
  title: PropTypes.string,
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

const ExpandBtn = styled.span`
  font-size: 13px;
  color:#2C3A57;
`;

const Title = styled.span`
  margin-left: 5px;
`;

const MethodItem = styled.button`
  width: 33.33%;
  text-align: center;
  border: 1px solid white;
  background-color: ${props => props.active ? '#2C3A57' : ''};
  color: ${props => props.active ? 'white' : ''};
  padding: 10px;
  @media (max-width: 768px) {
    font-size: 14px;
  }
  border-radius: 5px;
`;

const FormGroup = styled(Form.Group)`
  margin-right: 10px;
`;

const FormLabel = styled(Form.Label)`
  font-weight: bold;
`;

const Total = styled.div`
  border-top: ${props => props.mobile ? '' : '1px dashed black'};
  width: 100%;
  display: flex;
  flex-direction: column;
  display: none;
  @media (max-width: 768px) {
    display: inline;
  }
`;

const Cart = styled.div`
  width: 40%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  @media (max-width: 768px) {
    display: none;
  }
`;

const Btn = styled.div`
  padding: 20px 90px;
`;

const Content = styled.div`
  & .name {
    font-weight: bold;
  }
  width: 100%;
`;

const Container = styled.div`
  display: flex;
  flex-direction: row;
  height: 450px;
  @media (max-width: 768px) {
    height: 550px;
  }
`;

const Infor = styled.div`
  width: 60%;
  padding-right: 10px;
  margin-right: 10px;
  border-right: 1px solid #d1d1d1;
  @media (max-width: 768px) {
    width: 100%;
    padding-right: 0px;
    margin-right: 0px;
    border-right: none;
  }
`;
const CartItems = styled.div`
  width: 100%;
  overflow: scroll;
  padding-right: 1px;
  ${props => props.mobile ? 'width: 200px; height: 350px;' : ''};
`;

const Item = styled.div`
    display: flex;
    flex-direction: row;
    width: 100%;
    border-radius: 5px;
    padding: 5px;
    border: 1px solid #cccccc;
    ${props => props.expand ? 
      'margin-bottom: 5px;' : 
      'margin-bottom: 10px;'
    }
    
    & img {
      height: 50px;
      width: 50px;
      margin: auto 10px;
    }
`;


const PopupItem = styled(Modal)`
  & .my-modal {
    min-width: 50%;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: ${props => props.marginBottom ? props.marginBottom : ""};
  & .price {
    color: red;
    font-weight: bold;
  }
  & .price-large{
    color: red;
    font-weight: bold;
    font-size: 20px;
    @media (max-width: 768px) {
      font-size: 18px;
    }
  }
  & .price-mini {
    color: red;
  }
  & .title {
    color: #333333;
    font-weight: 600;
    font-size: 20px;
    @media (max-width: 768px) {
      font-size: 18px;
    }
  }
  & .tax {
    width: 100%;
    text-align: right;
  }
  & .expDate {
    width: 60%;
  }
  & .cvv {
    width: 40%;
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: ${props => props.align === "right" ? "right" : "left"}
`;


const Quantity = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
  font-weight: 300;
`;

export default Payment
