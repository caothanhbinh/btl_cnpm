import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {Modal} from 'react-bootstrap'
import Button from "./Button";
import 'bootstrap/dist/css/bootstrap.min.css';

const Popup = (props) => {

  const [show, setShow] = useState(false);
 
  useEffect(() => {
    setShow(props.show);
    var x = document.getElementsByClassName("checkbox");
    for(var i=0; i< x.length; i++) {
      x[i].checked = false;
    }
  }, [props.show]);

  const handleClose = () => {
    setShow(false);
    props.onClose(false);
  }

  return (
      <PopupItem show={show} onHide={handleClose}
        dialogClassName="my-modal"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>Add to cart</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Content>
          <Frame>
            <Square>
              <Img src={props.image} alt="food" />
            </Square>
          </Frame>
          <Container>
            <Detail>
              <Row>
                <Col>
                  <Field>Tên món ăn</Field>
                  <Text>{props.name}</Text>
                </Col>
                <Col align="right">
                  <Field>Đơn Giá</Field>
                  <Text color="red" weight="bold">{"$" + props.price}</Text>
                </Col>
              </Row>
              <Hr/>
              <Row border="yes">
                <Field>Số lượng</Field>
                <Quantity>
                  <Button onClick={props.onDec} text="-" buttonStyle="btn--decrease" buttonSize="btn--square"/>
                  <Input onChange={e => props.setQuantity(e.target.value)} value={props.quantity} type="text"/>
                  <Button onClick={props.onInc} text="+" buttonStyle="btn--increase" buttonSize="btn--square"/>
                </Quantity>
              </Row>
              <Hr/>
            <Sidedish>
              {Object.keys(props.description).map((field, value) => 
                <Row justify="12" key={field}>
                  <Field size="15px">{field}: <Text size={"15px"}>{props.description[field]}</Text></Field>
                </Row>
              )}
              
              <Field>Món phụ</Field>
              {(props.sideDishs).map((sideDish) => 
                <Row justify="no" key={sideDish.name}>
                  <Checkbox type="checkbox" className="checkbox" name={sideDish.name} value={sideDish.price} onChange={e => props.addSideDish(e)}/>
                  <Row justify="">
                    <label>{sideDish.name}</label>
                    <label className="price">{"$" + sideDish.price}</label>
                  </Row>
                </Row>
              )}
            </Sidedish>
            </Detail>
            <Button text="Thêm vào giỏ" buttonStyle="btn--add" buttonSize="btn--large" onClick={() => props.onAdd()}/>
          </Container>
        </Content>
        </Modal.Body>
      </PopupItem>
  );
};

Popup.propTypes = {
  title: PropTypes.string,
  show: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired
};

const Content = styled.div`
  display: flex;
  height: 90%;
  justify-content: space-between;
  box-sizing: border-box;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Sidedish = styled.div`
  height: 250px;
  overflow: scroll;
`;

const PopupItem = styled(Modal)`
  & .my-modal {
    min-width: 45%;
  }
`;

const Row = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: 100%;
  justify-content: ${props => props.justify ? "" : "space-between"};
  margin: 3px 0px;
  & .price {
    color: red;
    font-weight: bold;
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  text-align: ${props => props.align === "right" ? "right" : "left"}
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  
  box-sizing: border-box;
  @media (max-width: 768px) {
    
  }
`;

const Field = styled.span`
  font-size: ${props => (props.size) ? props.size : "20px"};
  font-weight: bold;
`;

const Text = styled.span`
  font-size: ${props => (props.size) ? props.size : "20px"};
  font-weight: ${props => (props.weight) ? props.weight : "normal"};
  color: ${props => (props.color) ? props.color : "black"};
`;

const Detail = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

const Img = styled.img`
  width: 100%;
  height: 100%;
`;

const Frame = styled.div`
  width: 30%;
  box-sizing: border-box;
  flex-grow: 1;
  padding-right: 20px;
  @media (max-width: 768px) {
    padding-right: 20px;
  }
`;

const Square = styled.div`
  border-radius: 15px;
  border: 2px solid #f2f2f2;
`;

const Quantity = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  box-sizing: border-box;
`;

const Input = styled.input`
  border: none;
  margin: 5px;
  height: 90%;
  width: 35px;
  text-align: center;
`;

const Checkbox = styled.input`
  zoom: 1.5;
  margin: 1px;
  margin-right: 5px;
`;

const Hr = styled.hr`
  width: 100%;
  background-color: gray;
`;

export default Popup;