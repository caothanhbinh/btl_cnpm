import React from 'react'
import styled from 'styled-components';
import { TopBar } from '../../components';

const Payment = () => {
  return (
    <Container>
      <LeftContainer>
        <TopBar icon="fa-arrow-left" title='Back'/>
        <Title>PAYMENT</Title>
        
      </LeftContainer>
    </Container>
  )
}

const Container = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 2px;
    margin-right: auto;
    margin-left: auto;
    width: 70%;
    height: 99vh;
    border-style: solid;
    border-width: 0.5px;
    border-color: #D2D2D2;
    box-shadow: 2px -2px 2px #D2D2D2;
    background-color: #F8F8F8;
    @media (max-width: 1024px) {
      width: 100%;
    }
`;

const LeftContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    @media (max-width: 768px) {
      width: 100%;
    }
`;

const Title = styled.span`
  padding: 5px;
  margin-left: 5px;
  border-radius: 7px;
  color: #2C3A57;
  text-align: center;
  font-size: 17px;
  font-weight: bold;
`;

export default Payment
