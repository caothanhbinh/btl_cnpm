import React, {useEffect, useState} from 'react';
import images from '../../components/images'
import { TopBar, ListType, ListFood, Cart, Popup } from '../../components';
import styled from 'styled-components';
import Payment from '../../components/Payment';

const Home = () => {
  var dataFood = [{
    'id': 0,
    'typeId': 0,
    'name': "Blue cake",
    'price': 100,
    'description': {
    },
    'sideDishs': [],
    'url': images.blue_cake
  },
  {
    'id': 1,
    'typeId': 0,
    'name': "Pink cake",
    'price': 10,
    'description': {
      protein: "100"
    },
    'sideDishs': [{
      'name': "Cherry",
      'price': 1
    },
    {
      'name': "Strawberry",
      'price': 1
    }],
    'url': images.pink_cake
  },
  {
    'id': 2,
    'typeId': 0,
    'name': "Love cake",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [{
      'name': "Chocolate",
      'price': 1
    }],
    'url': images.love_cake
  }, {
    'id': 3,
    'typeId': 1,
    'name': "Orange juice",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [{
      'name': "Sugar",
      'price': 1
    }],
    'url': images.orange
  }, {
    'id': 4,
    'typeId': 2,
    'name': "Redbull",
    'price': 13,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.redbull
  }, {
    'id': 5,
    'typeId': 2,
    'name': "Coca",
    'price': 12,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.coca
  },
  {
    'id': 6,
    'typeId': 2,
    'name': "Pepsi",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.pepsi
  },{
    'id': 7,
    'typeId': 3,
    'name': "Lobster",
    'price': 50,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.lobster
  },{
    'id': 8,
    'typeId': 3,
    'name': "Shrimp",
    'price': 60,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.shrimp
  },{
    'id': 9,
    'typeId': 3,
    'name': "Clam",
    'price': 55,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.clam
  },{
    'id': 10,
    'typeId': 4,
    'name': "Pasta",
    'price': 45,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.pasta
  },{
    'id': 11,
    'typeId': 4,
    'name': "Spaghetti",
    'price': 50,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.spaghetti
  },{
    'id': 12,
    'typeId': 5,
    'name': "Burger",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.burger
  },{
    'id': 13,
    'typeId': 5,
    'name': "Sandwich",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.sandwich
  }
  ,{
    'id': 14,
    'typeId': 5,
    'name': "KFC",
    'price': 40,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.chicken
  }
  ,{
    'id': 15,
    'typeId': 5,
    'name': "Fried chip",
    'price': 15,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.chip
  }
  ,{
    'id': 16,
    'typeId': 5,
    'name': "Water",
    'price': 5,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.water
  },{
    'id': 17,
    'typeId': 5,
    'name': "Pizza",
    'price': 45,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.pizza
  },{
    'id': 18,
    'typeId': 5,
    'name': "Hot dog",
    'price': 25,
    'description': {
      protein: "100",
      calo: "200"
    },
    'sideDishs': [],
    'url': images.hotdog
  }]

  var dataType = [{
    'id': 0,
    'url':images.choco_cake,
    "name": "Cupcake"
  },
  {
    'id': 1,
    'url':images.juice,
    "name": "Juice"
  },
  {
    'id': 2,
    'url':images.coca,
    "name": "Beverage"
  },
  {
    'id': 3,
    'url':images.seafood,
    "name": "Seafood"
  },
  {
    'id': 4,
    'url':images.noodle,
    "name": "Noodles"
  },
  {
    'id': 5,
    'url':images.fastfood,
    "name": "Fastfood"
  },{
    'id': 6,
    'url':images.coca,
    "name": "Coca"
  },
  {
    'id': 7,
    'url':images.coca,
    "name": "Coca"
  },
  {
    'id': 8,
    'url':images.coca,
    "name": "Coca"
  }]
  const [cartId,setCartId] = useState(0)
  const [id, setID] = useState(0)
  const [name, setName] = useState("")
  const [price, setPrice] = useState(0)
  const [isPopup, setIsPopup] = useState(false)
  const [isPayment, setIsPayment] = useState(false)
  const [url, setUrl] = useState("")
  const [description, setDescription] = useState({})
  const [quantity, setQuantity] = useState(0)
  const [sideDishs, setSideDishs] = useState([])
  const [sideDishsChecked, setSideDishsChecked] = useState([])
  const [cartItems, setCartItems] = useState([])
  const [foodDisplay, setFoodDisplay] = useState([])
  const [title, setTitle] = useState('')
  const [show, setShow] = useState(0)

  const onDisplay = (food) => {
    setIsPopup(true)
    setID(food.id)
    setName(food.name)
    setPrice(food.price)
    setUrl(food.url)
    setSideDishs(food.sideDishs)
    setDescription(food.description)
    setSideDishsChecked([])
    setQuantity(0)
  }

  const checkExist = (id, sideDishsChecked) => {
    const index = cartItems.findIndex(e => e.id === id && JSON.stringify(e.sideDishsChecked) === JSON.stringify(sideDishsChecked))
    if (index === -1) return false;
    return true;
  }
  const onAdd = () => {
    if(quantity > 0) {
      setIsPopup(false)
      if (checkExist(id, sideDishsChecked)) {
        const index = cartItems.findIndex(e => e.id === id && JSON.stringify(e.sideDishsChecked) === JSON.stringify(sideDishsChecked))
        const exist = cartItems.find(e => e.id === id && JSON.stringify(e.sideDishsChecked) === JSON.stringify(sideDishsChecked))
        setCartItems([...cartItems.slice(0,index), {...exist, quantity: exist.quantity + quantity}, ...cartItems.slice(index + 1)])
      }
      else setCartItems([...cartItems, {cartId, id, name, price, sideDishsChecked, url, quantity}]);
    }
    else alert("Bạn chưa nhập số lượng") 
  }
  const onAddOneItem = (food) => {
    if (checkExist(food.id, food.sideDishs)) {
      const index = cartItems.findIndex(e => e.id === food.id && JSON.stringify(e.sideDishsChecked) === JSON.stringify(food.sideDishs))
      const exist = cartItems.find(e => e.id === food.id && JSON.stringify(e.sideDishsChecked) === JSON.stringify(food.sideDishs))
      setCartItems([...cartItems.slice(0,index), {...exist, quantity: exist.quantity + 1}, ...cartItems.slice(index + 1)])
    }
    else setCartItems([...cartItems, {cartId: cartItems.length, id: food.id, name: food.name, price: food.price, sideDishsChecked: food.sideDishs, url: food.url, quantity: 1}]);
  }
  useEffect(() => {
    setCartId(cartItems.length)
  }, [cartItems])

  const onADD = (product) => {
    const exist = cartItems.find(x => x.cartId === product.cartId);
    setCartItems(cartItems.map(x => x.cartId === product.cartId ? { ...exist, quantity: exist.quantity + 1 } : x))
  }
  const onRemove = (product) => {
    const exist = cartItems.find((x) => x.cartId === product.cartId);
    if (exist.quantity === 1) {
      setCartItems(cartItems.filter((x) => x.cartId !== product.cartId));
    }
    else {
      setCartItems(
        cartItems.map((x) =>
          x.cartId === product.cartId ? { ...exist, quantity: exist.quantity - 1 } : x
        )
      );
  }
  }
  const closeModal = () => {
    setIsPopup(false)
  }
  const closePayment = () => {
    setIsPayment(false)
  }
  const onInc = () => {
    setQuantity(parseInt(quantity) +1)
  }
  const onDec = () => {
    if(quantity > 0) setQuantity(quantity - 1)
  }
  const addSideDish = (e) => {
    if(e.target.checked) {
      var name = e.target.name;
      var price = parseInt(e.target.value);
      var side = {'name': name, 'price': price}
      setSideDishsChecked([...sideDishsChecked, side]);
    }
    else {
      setSideDishsChecked(sideDishsChecked.filter(item => item.name !== e.target.name));
    }
  }
  const onDisplayType = (type) => {
    setTitle(type.name);
    setFoodDisplay(dataFood.filter((food) => food.typeId === type.id))
  }
  const displayCart = () => {
    setShow(!show);
  }
  const payment = () => {
    setIsPayment(true);
  }
  return (
    <Container>
        <LeftContainer>
            <TopBar onDisplay={displayCart} icon='fa-home' title='Back to home' numItem={cartItems.length}/>
            <ListType 
                data={dataType}
                onDisplay={onDisplayType}
            />
            <ListFood
                title={title}
                data={foodDisplay}
                onDisplay={onDisplay}
                onAdd={onAdd}
                onAddOneItem={onAddOneItem}
            />
            <Popup
              id={id}
              name={name}
              price={price}
              show={isPopup}
              image={url}
              description={description}
              quantity={quantity}
              sideDishs={sideDishs}
              onClose={closeModal}
              onInc={onInc}
              onDec={onDec}
              setQuantity={setQuantity}
              onAdd={onAdd}
              addSideDish={addSideDish}
            />
            <Payment
            show={isPayment}
            numItem={cartItems.length}
            onClose={closePayment}
            cartItems={cartItems}/>
          </LeftContainer>
        <Cart
        countCartItems={cartItems.length}
        onAdd={onADD}
        onRemove={onRemove}
        cartItems={cartItems}
        show={show}
        onDisplay={displayCart}
        payment={payment}
        />
      </Container>
  )
}
const Container = styled.div`
    display: flex;
    flex-direction: row;
    margin-top: 2px;
    margin-right: auto;
    margin-left: auto;
    width: 70%;
    height: 99vh;
    border-style: solid;
    border-width: 0.5px;
    border-color: #D2D2D2;
    box-shadow: 2px -2px 2px #D2D2D2;
    background-color: #F8F8F8;
    @media (max-width: 1024px) {
      width: 100%;
    }
`;

const LeftContainer = styled.div`
    display: flex;
    flex-direction: column;
    width: 70%;
    @media (max-width: 768px) {
      width: 100%;
    }
`;
export default Home;